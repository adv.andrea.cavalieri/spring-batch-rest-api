package com.github.nicoraynaud.batch.dto.mapping;

import com.github.nicoraynaud.batch.dto.StepExecutionDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.batch.core.StepExecution;

@Mapper(
    componentModel = "spring",
    uses = {DateMapper.class})
public interface StepExecutionMapper {

  @Mapping(source = "stepName", target = "name")
  @Mapping(source = "exitStatus.exitCode", target = "exitCode")
  @Mapping(source = "exitStatus.exitDescription", target = "exitMessage")
  StepExecutionDTO toDTO(StepExecution object);

  List<StepExecutionDTO> toDTO(List<StepExecution> object);
}
