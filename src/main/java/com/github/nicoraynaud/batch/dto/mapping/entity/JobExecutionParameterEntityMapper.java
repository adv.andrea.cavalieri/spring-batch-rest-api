package com.github.nicoraynaud.batch.dto.mapping.entity;

import com.github.nicoraynaud.batch.domain.entity.JobExecutionParameterEntity;
import com.github.nicoraynaud.batch.dto.JobExecutionParameterDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface JobExecutionParameterEntityMapper {

  @Mapping(source = "identifying", target = "identifyCharacter")
  JobExecutionParameterDTO toDTO(JobExecutionParameterEntity jobExecutionEntity);

  List<JobExecutionParameterDTO> toDTO(List<JobExecutionParameterEntity> jobExecutionEntity);
}
