package com.github.nicoraynaud.batch.dto.mapping;

import com.github.nicoraynaud.batch.domain.JobDescription;
import com.github.nicoraynaud.batch.dto.JobDescriptionDTO;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
    componentModel = "spring",
    uses = {JobDescriptionParameterMapper.class})
public interface JobDescriptionMapper {

  @Mapping(target = "lastExecutionStatus", ignore = true)
  JobDescriptionDTO toDTO(JobDescription jobDescription);

  List<JobDescriptionDTO> toDTO(List<JobDescription> jobDescription);
}
