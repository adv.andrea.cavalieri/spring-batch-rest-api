package com.github.nicoraynaud.batch.config.swagger.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/** Redirection to documentation. */
@Controller
public class SwaggerController {
  @GetMapping(path = "/swagger")
  public String index() {
    return "redirect:swagger-ui.html";
  }
}
