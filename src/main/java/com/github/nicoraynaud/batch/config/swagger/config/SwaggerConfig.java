package com.github.nicoraynaud.batch.config.swagger.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.classmate.TypeResolver;
import com.github.nicoraynaud.batch.config.swagger.plugin.PageableParameterBuilderPlugin;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.TypeNameExtractor;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
// @Profile(SpringProfiles.NOT_PRODUCTION)
//@ConditionalOnProperty(name = "app.swagger.enable", havingValue = "true")
@EnableSwagger2
public class SwaggerConfig {
  @Bean
  public Docket customImplementation() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
        .build();
  }

  @Bean
  @ConditionalOnMissingBean
  public PageableParameterBuilderPlugin pageableParameterBuilderPlugin(
      TypeNameExtractor typeNameExtractor, TypeResolver typeResolver) {
    return new PageableParameterBuilderPlugin(typeNameExtractor, typeResolver);
  }
}
